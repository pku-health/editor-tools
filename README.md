edit.me
-------------------
> 一个文本编辑系统。

## 为何要做这个项目？
这是为了在`五维十工程`实践中整一个更好的软件工具
> 五维十工程实践的过程是对健康整体状态的管理。

目前看来里面包括了量表填写与分析、日程管理、任务管理、时间统计、写总结等子项目。
主要是不能忍受现有工具。因为没有一个现有工具可以把上面的东西成体系的管理起来。都是零零散散。下面具体说说。

首先我们每人都有一个总结的文档，这是最初级的形式，现在想量化一些东西，看图表什么的，于是用上了百宝云，可大家仍然不放弃自己的总结文档，这是因为文档看内容（文字）比较方便，那么问题就来了，我们为了获得两边的便利性，需要手动去填表。这就需要花更多的时间，同时两边的时长数据是冗余（重复）的。那有没有一个办法，让我们能够容易的把图表和文字结合起来看，同时又提供单独的图表视图呢？

这只是其中一角，实际上数据冗余更多——每天的总结里有明日计划，这个和钉钉的项目待办列表有什么区别吗？那为了用钉钉的待办列表功能（可能是为了共享进度和分配任务）我们还需要把总结里的明日计划再复制粘贴到钉钉里去

这还不算完，之前睿麒让我记录每天分配的时间，相当于是日程规划的功能，还得那边填完表回到这边统计时长数据，本来是同一个东西，就因为不同人对于粒度要求不同，结果要填两遍——然后之前日程规划的时候，本来可以和任务（待办列表）进行一个很好的结合，但现在的工具结合不起来，还需要手动复制粘贴，所以最后我填了一段时间后完全放弃了待办列表和日程——类似的问题可以一直说下去。

与此类似的还有文献管理的问题，文献管理我就没觉得有一个工具让我满意的，不知道你们用什么工具，我这边有的师兄是有道云笔记（写阅读笔记）+文件夹管理。我自己用zotero+坚果云+为知笔记。你们可能用noteexpress……这往大了说是信息孤岛的数据互通问题，历史上无数人试图解决，然并卵。好消息是我们如果能画一个框（圈定需求），那总是可以解决问题，坏消息是框内和框外永远存在数据互通的问题。

所以我们解决的思路是什么呢？首先肯定是要有一个框把目前能想到的东西框起来统一整合解决，同时提供对外接口保持开放性。

那么框内的东西要如何解决呢？其实是要解决2个问题：1.允许用户自定义视图，2.同一份数据在不同的视图中有不同的表达（展示）形式。这么看起来的话，和普通的web编程也没什么差别。那么难点在什么地方呢？难点在于不能提前

比如现在的话，需要有这么几个视图：总结视图，任务视图，时间管理视图，报表视图。那么又有哪些数据类型呢？总结的普通文本数据，时间统计数据（起点、终点、时长、内容），任务数据（标题、内容、上级任务）。难点有2个，一个是随时间热度的推荐，一个是数据引用的问题。还有一个搜索的问题。最后还有一个用TGraph实现的问题。


### 技术开发路线

兵分两路：一路是添加和使用现有ckeditor中的功能组件，比如图片粘贴和自动上传，Word拷贝自动格式化，多人协同编辑等等；另一路是自己开发插件，实现高级自定义功能。

自己开发插件的话，首先是需要一个标记文本的功能（标签？）。可以标记一段文本内容并添加一个对应的object{key:value}（含多个键值对）。有什么用呢？一个是这个元信息……然后就是这个标签的展示
╮(╯▽╰)╭哎这个又开始完美主义的问题了，对于五维十工程来说，目前其实想解决的就是这么几个事情，
1. 时间统计。需要有个标签来格式化组织这个信息。方便用程序自动提取。然后需要有一个挂件能够展示和统计（类似于百宝云，只不过是精简版的图表）
2. 任务。这个相对好说，主要还是需要在多用户之间共享。
3. 能在时间统计里引用任务信息（就是引用，并且把对应的时间加到任务中）。
4. 日程视图。

……说白了这几个都是相当于有一个视图（挂件）只不过可以把挂件中的信息插入到文本中来展示。说白了是比较像百宝云里面的那个导出报表的功能，把

如果是做那个文献阅读之类的事情的话，需要的包括：
1. 和上面类似的引用能力
2. 版本化能力，类似于git，又如Word的审核模式
3. 按标记组织文档显示结果（即把文档中的文本按照标记进行查询，排序，显示/隐藏）这个的用处是什么？

如果不考虑3的话，那么其实最佳的技术路线是用GreaseMonkey之类的编写脚本实现。但是考虑到现在大家使用的底层技术栈，要么用svg（金山在线文档），要么是quilljs（石墨这个还好说），要么是meloXXX（腾讯文档，排版巨复杂），
所以就是特别希望能把这个外部信息引入做成可插入式的，让普通人也能上手自己搞个插件（自定义插件可能太难了，但是自定义个数据结构还是可以的，特别是提供了后台的情况下。唔说白了是可以在后台创建类/对象，然后在文档中引用，在文档中编辑的时候，也同时修改后台的内容）
不管了，先搞特殊化，再搞通用化。
实现时间统计的功能。这个功能的话，数据结构是含有一个开始时间和结束时间，以及简单说明（详细说明放在文档里吧），创建的时候那个时间可以选（选其实不方便，还是整个日程那种拖动选择比较方便。或者说如果在文档里创建就选择，在旁边的日程组件创建就拖动），如果在文档中创建要同步到旁边的日程组件中去。在日程组件中创建成功后，可以拖拽到文本中（引用）。同时日程旁边应该还有个饼图的统计，也可以拖到文本中（引用一个求和的结果）。

这个功能含有一个工具栏按钮，点击后会在光标处插入一个时间统计tag，有start、end和一个text说明。text处可以输入文字内容。同时边栏弹出日程视图并在上面具象一个滑块表示时间消耗（自动接着上一块，时间最小单位是5分钟），可以拖拽滑块的上下边界来调整时间范围。输入文字后边栏的滑块上会同步显示文字。点击边栏的滑块后文档自动滚动到相应位置。拖拽滑块到文档中可以引用这个内容（这里有个问题是是否允许多次引用？理论上可以有多次）

那我是不是得先写一个边栏的组件（类似于intellij idea的那种上下左右都可以弹出和收回的边栏，看看有没有现成的插件吧，抽屉如何？不好，不能在文档中输入文字，但是暂时用来应急是可以的）边栏用抽屉。
日程视图需要自己写一个控件了(想过table+滑动输入条slider，但是slider没法容纳多个activity，所以要么table合并单元格，要么自己用div从头写)。最好可以用键盘操作，上下整体调整，左调整上边界，右调整下边界。同时点击可以编辑内容，内容太长的省略号加tooltip显示，编辑的时候要不要用自动加长的？。点鼠标拖拽可以具象一个滑块。

toolbar图标是20x20的

```
npm install   # Project setup
npm run serve # Compiles and hot-reloads for development
npm run build # Compiles and minifies for production
npm run lint  # Lints and fixes files
```
