// 基础功能组件
import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
// 按钮视图组件
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview'

// 按钮的图标 SVG  <!-- Created with Method Draw - http://github.com/duopixel/Method-Draw/ -->
const toolbarBtnIcon = `<svg width="20" height="20" xmlns="http://www.w3.org/2000/svg">
 <g>
  <title>background</title>
  <rect fill="#fff" id="canvas_background" height="22" width="22" y="-1" x="-1"/>
 </g>
 <g>
  <title>Layer 1</title>
  <text transform="matrix(0.857924, 0, 0, 0.596372, 2.51765, 11.8482)" xml:space="preserve" text-anchor="start" font-family="Helvetica, Arial, sans-serif" font-size="24" id="svg_1" y="8.88289" x="5.73784" stroke-width="0" stroke="#000" fill="#000000">A</text>
  <text transform="matrix(2.35135, 0, 0, 1.24149, 15.8621, 31.6599)" xml:space="preserve" text-anchor="start" font-family="Helvetica, Arial, sans-serif" font-size="24" id="svg_2" y="-6.71671" x="-11.85517" fill-opacity="null" stroke-opacity="null" stroke-width="0" stroke="#000" fill="#000000">T</text>
 </g>
</svg>
`

// 自定义组件需要继承基础组件
export default class Activity extends Plugin {
  // 重写基础组件的初始化函数
  init() {
    // 获取当前编辑器
    const editor = this.editor
    // 从当前编辑器的配置中获取传入的自定义配置名称
    const fullscreen = this.editor.config.get('fullscreen')

    // 将自定义按钮添加到当前编辑器的组件工厂
    editor.ui.componentFactory.add('activity', locale => {
      // 获取当前编辑器的按钮视图
      const view = new ButtonView(locale)

      // 在按钮视图中注入自定义按钮的名称、图标
      view.set({
        label: 'activity',
        icon: toolbarBtnIcon,
        // 开启提示，当鼠标悬浮到按钮上，会显示 label 指定的 fullscreen 字样
        tooltip: true
      })

      // 点击按钮
      view.on('execute', () => {
        // 执行自定义配置中指定的回调函数
        fullscreen.handler.call()
      })

      // 返回修改后的视图内容
      return view
    })
  }
}