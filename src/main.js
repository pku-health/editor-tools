import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';


// Vue.use( CKEditor );
Vue.config.productionTip = false
Vue.use(Antd)
Vue.use(Vuex)

new Vue({
	render: h => h(App),
}).$mount('#app')
